var dotenv = require('dotenv');
dotenv.config({path: '../.env'});

var mysql = require('mysql');
var connection;

if (process.env.JAWSDB_URL){
    connection = mysql.createConnection(process.env.JAWSDB_URL);
} else {
    connection = mysql.createConnection({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: process.env.DB_PASS,
        database: 'burgers_db'
    });
}

connection.connect(function(error){
    if (error) {
        console.log(`Trouble connecting to port - ${error.stack}`);
        return;
    }
    console.log(`Connected as id ${connection.threadId}`);
});

module.exports = connection;