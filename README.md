# burger

### Summary
The burger app logs burgers added via a form on the page and creates a button, which when clicked then "devours" the burger, moving it to the devoured section.

### Tools used
Model-View-Controller design pattern <br>
**Model/Controller:** Node, Express, MySQL, SQL queries <br>
**View:** Handlebars, Materialize, CSS, jQuery, Google Fonts <br>

### How to use
Navigate to the app's [homepage](https://fast-harbor-95128.herokuapp.com/).

![Burger homepage](./images/main.png)

Add a burger using the form:

<img src="./images/enter.png" alt="Burger enter" title="Burger enter" width="400" />

Click submit and a button will be created for your burger.

<img src="./images/add.png" alt="Burger add" title="Burger add" width="400" />

Click on any of the burger buttons to devour that burger.

<img src="./images/clicked.png" alt="Burger clicked" title="Burger clicked" width="400" />