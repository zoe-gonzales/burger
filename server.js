// express dependency
var express = require('express');

// saving express function to variable
var app = express();

// port app will listen on
var PORT = process.env.PORT || 3000;

// serving static files
app.use(express.static("public"));

// parsing content
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// handlebars dependency
var exphbs = require("express-handlebars");

// setting up handlebars as view engineer
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

// router dependency from controllers dir
var router = require('./controllers/burgers_controller');

// integrating with app
app.use(router);

// listening on port
app.listen(PORT, function(){
    console.log(`hanging out on port ${PORT}`);
});