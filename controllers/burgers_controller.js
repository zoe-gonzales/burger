var express = require('express');
var burger = require('../models/burger');
var router = express.Router();

// get all burgers from db
router.get('/', function(req, res){
    burger.all(function(data){
        var burgersObj = {
            burgers: data
        }
        res.render('index', burgersObj);
    });
});

// add one burger to db
router.post('/api/burgers', function(req, res){
    burger.add(
        req.body.burger_name, 
        false,
        function(data){
            res.json({id: data.insertId});
        }
    );
});

// update devoured status of burger
router.put('/api/burgers/:id', function(req, res){
    burger.update(
        req.body.devoured,
        req.params.id, 
        function(data){
            if (data.changedRows === 0) {
                return res.status(404).end();
            }
            res.status(200).end();
        }
    );
});

module.exports = router;